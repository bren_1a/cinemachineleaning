﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Cinemachine;

public class TargetConroller : MonoBehaviour
{
    public GameObject vCam04;
    CinemachineVirtualCamera Cam04Virtual;

    CinemachineBasicMultiChannelPerlin cameraShake;

    public List<GameObject> targets;

    // Start is called before the first frame update
    void Start()
    {
        Cam04Virtual = vCam04.GetComponent<CinemachineVirtualCamera>();
        cameraShake = Cam04Virtual.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        cameraShake.m_AmplitudeGain = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Cam04Virtual.m_Follow = targets[0].transform;
            Cam04Virtual.m_LookAt = targets[0].transform;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Cam04Virtual.m_Follow = targets[1].transform;
            Cam04Virtual.m_LookAt = targets[1].transform;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            cameraShake.m_AmplitudeGain = 2;

            StartCoroutine(StopCameraShake());
        }
    }

    private IEnumerator StopCameraShake()
    {
        yield return new WaitForSeconds(2.5f);

        cameraShake.m_AmplitudeGain = 0;
    }
}
