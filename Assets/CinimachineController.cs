﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CinimachineController : MonoBehaviour
{
    PlayableDirector PD;
    public List<TimelineAsset> timelines;


    // Start is called before the first frame update
    void Start()
    {
        PD = GetComponent<PlayableDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            
            PD.Play(timelines[0]);
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            PD.Play(timelines[1]);
        }
    }
}
